#include "displayopenhmd.h"
#include <QPainter>

DisplayOpenHMD::DisplayOpenHMD(QWidget* _parent,QString _imageName)
{
    imageName = _imageName;
    this->setParent(_parent);
    QString imageNameString = QString("images/%0").arg(imageName);
    calibrationImage = new QImage(imageNameString);
    printf("loading image from %s\n",imageNameString.toUtf8().data());
    if (calibrationImage->width() == 0)
    {
        printf("couldnt open calibration image\n");
        exit(0);
    }
    //swapImageTimeout = 2000.0f; // every 2 seconds move our checkerboard image
    //swapImageTimer=0.0f;
}

void DisplayOpenHMD::setImageName(QString _imageName)
{
    imageName = _imageName;
    QString imageNameString = QString("images/%0").arg(imageName);
    if (calibrationImage)
    {
        delete (calibrationImage);
    }
    calibrationImage = new QImage(imageNameString);
    printf("loading image from %s\n",imageNameString.toUtf8().data());
    this->repaint();
}

void DisplayOpenHMD::update()
{
    this->repaint();
}

void DisplayOpenHMD::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setRenderHint(QPainter::HighQualityAntialiasing);
    //just randomly move the image around
    /*if (swapImageTimer > swapImageTimeout)
    {
        srand (time(NULL));
        xOff = rand() % (960-checkerboardImage->width());
        yOff = rand() % (1080-checkerboardImage->height());
        swapImageTimer = 0.0f;
    }*/
    painter.drawImage(xOff,yOff,*calibrationImage);
    painter.drawImage(xOff+960,yOff,*calibrationImage);
    //printf("moving checkerboard to: %d %d\n",xOff,yOff);
    //swapImageTimer += 100.0f;

}
