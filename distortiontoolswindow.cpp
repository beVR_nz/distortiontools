#include "distortiontoolswindow.h"
#include "ui_distortiontoolswindow.h"
#include "displayopenhmd.h"
#include <QDir>
#include <QString>
#include <QTimer>
#include <QSettings>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileDialog>
#include <QWindow>



DistortionToolsWindow::DistortionToolsWindow(QWidget *parent) : QMainWindow(parent) , ui(new Ui::DistortionToolsWindow)
{
    distortionCalculated = true;
    imagesCaptured=0;
    staticGridSize=12;

    cap = NULL;
    captureActive=false;

    //Load our application settings




    // Load our UI and connect signals/slots
    ui->setupUi(this);

    //ui->hmdDisplayCombo->addItem("grid8x8.png");
    //ui->hmdDisplayCombo->setCurrentIndex(0);

    connect(ui->captureNextButton,SIGNAL(pressed()),this,SLOT(captureNextImage()));
    //connect(ui->lensCaptureNextButton,SIGNAL(pressed()),this,SLOT(lensCaptureNextImage()));
    connect(ui->calculateDistortionButton,SIGNAL(pressed()),this,SLOT(calculateDistortion()));
    connect(ui->applyManualButton,SIGNAL(pressed()),this,SLOT(applyManual()));
    connect(ui->resetButton,SIGNAL(pressed()),this,SLOT(resetImageCapture()));
    //connect(ui->lensResetButton,SIGNAL(pressed()),this,SLOT(lensResetImageCapture()));
    connect(ui->saveButton,SIGNAL(pressed()),this,SLOT(saveData()));
    connect(ui->savePNGButton,SIGNAL(pressed()),this,SLOT(savePNG()));
    connect(ui->addProfileButton,SIGNAL(pressed()),this,SLOT(addCameraProfile()));
    connect(ui->showGridCheckbox,SIGNAL(stateChanged(int)),this,SLOT(showGridOverlay(int)));
    connect(ui->cameraSpinBox,SIGNAL(valueChanged(int)),this,SLOT(resetOpenCVCapture()));
    connect(ui->chessX,SIGNAL(valueChanged(int)),this,SLOT(resetChessboard()));
    connect(ui->chessY,SIGNAL(valueChanged(int)),this,SLOT(resetChessboard()));
    connect(ui->reloadProfileButton,SIGNAL(pressed()),this,SLOT(reloadProfile()));

    //connect(ui->calculateLensDistortionButton,SIGNAL(pressed()),this,SLOT(calculateLensDistortion()));
    //connect(ui->applyLensManualButton,SIGNAL(pressed()),this,SLOT(lensApplyManual()));
    //connect(ui->lensCropX,SIGNAL(editingFinished()),this,SLOT(setUIDirty()));
    //connect(ui->lensCropY,SIGNAL(editingFinished()),this,SLOT(setUIDirty()));
    //connect(ui->lensOffsetX,SIGNAL(editingFinished()),this,SLOT(setUIDirty()));
    //connect(ui->lensOffsetY,SIGNAL(editingFinished()),this,SLOT(setUIDirty()));
    //connect(ui->k1SpinBox,SIGNAL(valueChanged(double)),this,SLOT(lensApplyManual()));
    //connect(ui->k2SpinBox,SIGNAL(valueChanged(double)),this,SLOT(lensApplyManual()));
    //connect(ui->k3SpinBox,SIGNAL(valueChanged(double)),this,SLOT(lensApplyManual()));
    //connect(ui->t1SpinBox,SIGNAL(valueChanged(double)),this,SLOT(lensApplyManual()));
    //connect(ui->t2SpinBox,SIGNAL(valueChanged(double)),this,SLOT(lensApplyManual()));


    // only works for square matrices
    uint32_t sizeSqroot = int(sqrt(INTRINSICS_SIZE));
    intrinsic = cv::Mat(sizeSqroot, sizeSqroot, CV_64F,0.0);
    distCoeffs = cv::Mat(DISTORTION_SIZE,1,CV_64F,0.0);
    lensIntrinsic = cv::Mat(sizeSqroot, sizeSqroot, CV_64F,0.0);
    lensDistCoeffs = cv::Mat(DISTORTION_SIZE,1,CV_64F,0.0);

    reloadProfile();

    resetOpenCVCapture();

    CameraProfile* cp = &(cameraProfiles[ui->profileCombo->currentIndex()]);

    for (uint8_t i=0;i<INTRINSICS_SIZE;i++)
    {
        intrinsic.ptr<double>(i/3)[i%3] = cp->intrinsics[i];
    }
    for (uint8_t i=0;i<DISTORTION_SIZE;i++)
    {
        distCoeffs.ptr<double>(i)[0] = cp->distortionCoeffs[i];
    }

    //ui->imageSplitRightRadio->setChecked(false);
    //ui->imageSplitLeftRadio->setChecked(false);
    //ui->imageSplitFullRadio->setChecked(false);

    //switch (cp->split)
    //{
    //    case 0:
    //        ui->imageSplitFullRadio->setChecked(true);
    //        break;
    //    case 1:
    //        ui->imageSplitLeftRadio->setChecked(true);
    //        break;
    //    case 2:
    //        ui->imageSplitRightRadio->setChecked(true);
    //        break;
    //    default:
    //        printf("unknown split state! setting full\n");
    //        ui->imageSplitFullRadio->setChecked(true);
    //}



    uiDirty=true;

   resetChessboard();

   /* lensBoardSize.width=3;
    lensBoardSize.height=3;
    for(uint16_t i=0; i < lensBoardSize.width * lensBoardSize.height; i++)
    {
        lensFixedChessboardPoints.push_back(cv::Point3f(i/lensBoardSize.width, i%lensBoardSize.width, 0.0f));
    }*/


    //DisplayOpenHMD* fullscreenDisplay = new DisplayOpenHMD(NULL,ui->hmdDisplayCombo->currentText());
    //fullscreenDisplay->show();
    //if (qApp->screens().size() > 1)
    //{
    //    fullscreenDisplay->windowHandle()->setScreen(qApp->screens()[1]);
    //    fullscreenDisplay->showFullScreen();
    //}
    //else
    //{
    //    fullscreenDisplay->resize(800,600);
    //    printf("Could not find second display. manually position/fullscreen window.\n");
    //}

    //Update loop to repaint capture window - currently invoked at a fixed 60hz
    QTimer* timer = new QTimer();
    timer->setInterval(16);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    //connect(timer, SIGNAL(timeout()), fullscreenDisplay, SLOT(update()));
    timer->start();
}

DistortionToolsWindow::~DistortionToolsWindow()
{
    delete settings;
    delete ui;
}

void DistortionToolsWindow::setUIDirty()
{
    uiDirty = true;
}

void DistortionToolsWindow::reloadProfile()
{
    QString settingsFilePath = QDir(QApplication::applicationDirPath()).filePath("cameraprofiles.ini");
    settings = new QSettings(settingsFilePath,QSettings::NativeFormat);


    QString settingsText = QString(settings->value("cameraProfiles","").toString());
    distortionCalculated = false;
    cameraProfiles.clear();
    bool delayedIntrinsics=false;
    if (! ps.fromString(&cameraProfiles,settingsText))
    {
          printf("WARNING: could not load camera profile.\n");
          //create a new default camera profile
          CameraProfile cp;
          memset(&cp,0,sizeof(CameraProfile));
          cp.intrinsics[0]=1.0;
          cp.intrinsics[4]=1.0;
          cp.intrinsics[2]=0.5;
          cp.intrinsics[5]=0.5;
          cp.intrinsics[8]=1.0;

          strcpy(cp.name,"default");
          cameraProfiles.push_back(cp);
          delayedIntrinsics = true;


    }
    ui->profileCombo->clear();
    for (uint32_t i=0;i<cameraProfiles.size();i++)
    {
        CameraProfile* cp = &(cameraProfiles.at(i));
        ui->profileCombo->insertItem(i,QString(cp->name));
    }
    if (delayedIntrinsics)
    {
        resetIntrinsics();
    }
    uiDirty = true;
    update();
    applyManual();

}

void DistortionToolsWindow::saveData()
{
    //saveProfiles();
    QString qs = ps.toString(&cameraProfiles);
    settings->setValue("cameraProfiles",qs);
    settings->sync();
    ui->statusBar->showMessage("profile data saved.",5000);
}


void DistortionToolsWindow::captureNextImage()
{
    //distortionCalculated=false;
    if (imagesCaptured == 0)
    {
        objectPoints.clear();
    }

    cv::Mat grayImage;
    cv::Mat boardPoints;
    cv::cvtColor(image,grayImage,cv::COLOR_RGB2GRAY);
    bool found = cv::findChessboardCorners( grayImage, boardSize, boardPoints, cv::CALIB_CB_ADAPTIVE_THRESH );
    if (found)
    {
        ui->statusBar->showMessage("Chessboard pattern found in image.",2000);
        cv::cornerSubPix(grayImage, boardPoints, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(2, 30, 0.1));
        imagePoints.push_back(boardPoints);
        objectPoints.push_back(fixedChessboardPoints);
        imagesCaptured++;
        ui->imagesCapturedLabel->setText(QString("Images captured: %1").arg(imagesCaptured));
    }
    else
    {
        ui->statusBar->showMessage("Could not find Chessboard pattern in image.",2000);
    }
}

void DistortionToolsWindow::calculateDistortion()
{
    cv::Mat rvecs;
    cv::Mat tvecs;
    //aspect (pixel aspect?) initialised to 1s
    CameraProfile* cp = &(cameraProfiles.at(ui->profileCombo->currentIndex()));

    if (objectPoints.size() > 2)
    {
        int flags = cv::fisheye::CALIB_RECOMPUTE_EXTRINSIC;
        if (ui->skewBox->isChecked()) {
            flags |= cv::fisheye::CALIB_FIX_SKEW;
        }
        float rpError = cv::fisheye::calibrate(objectPoints, imagePoints, cv::Size(image.cols,image.rows), intrinsic, distCoeffs, rvecs, tvecs,flags);
        ui->statusBar->showMessage(QString("Lens Distortion Calculated. Reprojection Error: %1").arg(rpError),5000);
       cp->reprojectionError  = rpError;
    }
    else
    {
        ui->statusBar->showMessage("No Calibration data. Capture at least 3 checkerboard images.",5000);
    }
    for (uint8_t i=0;i<INTRINSICS_SIZE;i++)
    {
        cp->intrinsics[i]=intrinsic.at<double>(i/3,i%3);
    }
    for (uint8_t i=0;i<DISTORTION_SIZE;i++)
    {
        cp->distortionCoeffs[i]=distCoeffs.at<double>(i,0);
    }
    //intrinsic.at<double>(0,1) = 0.0f;
    //cp->intrinsics[1] = 0.0f; //fix weirdness from fisheye calibrate
    distortionCalculated = true;
    uiDirty=true;


}

void DistortionToolsWindow::applyManual()
{
    CameraProfile* cp = &(cameraProfiles.at(ui->profileCombo->currentIndex()));

    //parse the values in the text box and copy to the profile

    QString intrinsicsString = ui->intrinsicsText->document()->toPlainText();
    QString distString = ui->distortionText->document()->toPlainText();
    intrinsicsString.replace("\n"," ");
    QStringList intrinsicsSplit = intrinsicsString.split(" ");
    QStringList distSplit = distString.split(",");

    float parsedIntrinsics[INTRINSICS_SIZE];
    float parsedDist[DISTORTION_SIZE];

    QString el;
    uint8_t c=0;
    foreach(el,intrinsicsSplit)
    {
       parsedIntrinsics[c] = intrinsicsSplit.at(c).toFloat();
       c++;
    }
    c=0;
    foreach(el,distSplit)
    {
       parsedDist[c] = distSplit.at(c).toFloat();
       c++;
    }


    for (uint8_t i=0;i<INTRINSICS_SIZE;i++)
    {
        cp->intrinsics[i]=parsedIntrinsics[i];
        intrinsic.at<double>(i/3,i%3) = parsedIntrinsics[i];
    }
    for (uint8_t i=0;i<DISTORTION_SIZE;i++)
    {
        cp->distortionCoeffs[i]=parsedDist[i];
        distCoeffs.at<double>(i,0) = parsedDist[i];
    }

    distortionCalculated = true;
    uiDirty=true;


}


/*void DistortionToolsWindow::lensApplyManual()
{
    QString intrinsicsString = ui->lensIntrinsicsText->document()->toPlainText();

    QString distString = QString("%1,%2,%3,%4")
            .arg(ui->k1SpinBox->value(),0,'f',4)
            .arg(ui->k2SpinBox->value(),0,'f',4)
            .arg(ui->t1SpinBox->value(),0,'f',4)
            .arg(ui->t2SpinBox->value(),0,'f',4);
            //.arg(ui->k3SpinBox->value(),0,'f',4);
    ui->lensDistortionText->setPlainText(distString);

    intrinsicsString.replace("\n"," ");
    QStringList intrinsicsSplit = intrinsicsString.split(" ");
    QStringList distSplit = distString.split(",");

    float parsedIntrinsics[INTRINSICS_SIZE];
    float parsedDist[DISTORTION_SIZE];

    QString el;
    uint8_t c=0;
    foreach(el,intrinsicsSplit)
    {
       parsedIntrinsics[c] = intrinsicsSplit.at(c).toFloat();
       c++;
    }
    c=0;
    foreach(el,distSplit)
    {
       parsedDist[c] = distSplit.at(c).toFloat();
       c++;
    }


    for (uint8_t i=0;i<INTRINSICS_SIZE;i++)
    {
        lensIntrinsic.at<double>(i/3,i%3) = parsedIntrinsics[i];
    }
    for (uint8_t i=0;i<DISTORTION_SIZE;i++)
    {
        lensDistCoeffs.at<double>(i,0) = parsedDist[i];
    }

    distortionCalculated = true;
    uiDirty=true;


}
*/

void DistortionToolsWindow::resetChessboard() {
    // initialise OpenCV data for both passes - chessboard is setup static w/regard to world.
    boardSize.width=ui->chessX->value();
    boardSize.height=ui->chessY->value();
    fixedChessboardPoints.clear();
    for(uint16_t i=0; i < boardSize.width * boardSize.height; i++)
    {
        fixedChessboardPoints.push_back(cv::Point3f(i/boardSize.width, i%boardSize.width, 0.0f));
    }
    resetImageCapture();
}

void DistortionToolsWindow::resetOpenCVCapture()
{
    captureMutex.lock();
    ui->cameraSpinBox->setEnabled(false);
    CameraProfile* cp = &(cameraProfiles.at(ui->profileCombo->currentIndex()));
    if (cap) {
        cap->release();
        delete(cap);
        cap=NULL;
        captureActive=false;
        image.release();
    }

    cap = new cv::VideoCapture(ui->cameraSpinBox->value());

    if(!cap->isOpened())
    {
         printf("ERROR: could not open video capture device.\n");
        ui->statusBar->showMessage("ERROR: Could not open selected Camera!");
        captureActive = false;
    }
    else {
        int captureFPS=cap->get(cv::CAP_PROP_FPS);
        if (captureFPS > 0)
        {
            captureWidth=cap->get(cv::CAP_PROP_FRAME_WIDTH);
            captureHeight=cap->get(cv::CAP_PROP_FRAME_HEIGHT);

            captureActive=true;
        }
        else
        {
            ui->statusBar->showMessage("ERROR: Could not query camera!");
            captureActive = false;
        }
    }
    uiDirty = true;
    ui->cameraSpinBox->setEnabled(true);
    captureMutex.unlock();
    reloadProfile();
    if (int(cp->captureSize[0]) != captureWidth || int(cp->captureSize[1]) != captureHeight)
    {
        resetIntrinsics();
    }

}


void DistortionToolsWindow::resetIntrinsics()
{
    CameraProfile* cp = &(cameraProfiles.at(ui->profileCombo->currentIndex()));
    for (uint8_t i=0;i<INTRINSICS_SIZE;i++)
    {
        cp->intrinsics[i]=0.0f;
    }

    cp->intrinsics[0]=captureWidth; // reset fx to something displayable
    cp->intrinsics[4]=captureWidth; // reset fy to something displayable
    cp->intrinsics[2]=captureWidth/2.0f; // reset cx to something displayable
    cp->intrinsics[5]=captureHeight/2.0f; // reset cy to something displayable
    cp->intrinsics[8]=1.0f; // reset fy to something displayable

}

void DistortionToolsWindow::resetImageCapture()
{
    imagesCaptured = 0;
    imagePoints.clear();
    objectPoints.clear();

    resetIntrinsics();
    CameraProfile* cp = &(cameraProfiles.at(ui->profileCombo->currentIndex()));

    for (uint8_t i=0;i<DISTORTION_SIZE;i++)
    {
        cp->distortionCoeffs[i]=0.0f; //5 element flat array
    }

    for (uint8_t i=0;i<INTRINSICS_SIZE;i++)
    {
        intrinsic.ptr<double>(i/3)[i%3] = cp->intrinsics[i]; //copy 9-element flat array to 3x3 square cv::Mat
    }
    for (uint8_t i=0;i<DISTORTION_SIZE;i++)
    {
        distCoeffs.ptr<double>(i)[0] = cp->distortionCoeffs[i]; // 5x1 cv::Mat
    }

    ui->statusBar->showMessage("Calibration data cleared.",5000);
    ui->imagesCapturedLabel->setText(QString("Images captured: %1").arg(imagesCaptured));
    cp->reprojectionError = 0.0f;
    uiDirty=true;
}

/*void DistortionToolsWindow::lensResetImageCapture()
{
    lensImagesCaptured = 0;
    lensImagePoints.clear();
    lensObjectPoints.clear();

    for (uint8_t i=0;i<DISTORTION_SIZE;i++)
    {
        lensDistCoeffs.ptr<double>(i)[0] = 0.0; // 5x1 cv::Mat
    }

    ui->statusBar->showMessage("Lens data cleared.",5000);
    ui->imagesCapturedLabel->setText(QString("Images captured: %1").arg(lensImagesCaptured));
    uiDirty=true;
}
*/
void DistortionToolsWindow::update()
{
    captureMutex.lock();
    CameraProfile* cp = &cameraProfiles.at(ui->profileCombo->currentIndex());
    QString formattedIntrinsics = QString("%1 %2 %3\n%4 %5 %6\n%7 %8 %9")
            .arg(cp->intrinsics[0],0,'f',4).arg(cp->intrinsics[1],0,'f',4).arg(cp->intrinsics[2],0,'f',4)
            .arg(cp->intrinsics[3],0,'f',4).arg(cp->intrinsics[4],0,'f',4).arg(cp->intrinsics[5],0,'f',4)
            .arg(cp->intrinsics[6],0,'f',4).arg(cp->intrinsics[7],0,'f',4).arg(cp->intrinsics[8],0,'f',4);
    QString formattedDistortion = QString("%1,%2,%3,%4") //,%5")
            .arg(cp->distortionCoeffs[0],0,'f',4)
            .arg(cp->distortionCoeffs[1],0,'f',4)
            .arg(cp->distortionCoeffs[2],0,'f',4)
            .arg(cp->distortionCoeffs[3],0,'f',4);
            //.arg(cp.distortionCoeffs[4],0,'f',4);

    if (captureActive)
    {

/*  if (uiDirty)
    {
        if (ui->autoIntrinsicsCheckbox->checkState() == Qt::CheckState::Checked)
        {
            ui->lensIntrinsicsText->setText(formattedIntrinsics);
            if (ui->lensDistortionText->toPlainText().compare("") == 0)
            {
                ui->lensDistortionText->setText("0.0,0.0,0.0,0.0");
            }
            lensApplyManual();
            std::cout << lensIntrinsic;
            ui->lensCropX->setValue(lensIntrinsic.at<double>(0,0));
            printf("WAT: %f\n",lensIntrinsic.at<double>(0,0));
            ui->lensCropY->setValue(lensIntrinsic.at<double>(1,1));
            ui->lensOffsetX->setValue(lensIntrinsic.at<double>(0,2) - lensIntrinsic.at<double>(0,0) /2 );
            ui->lensOffsetY->setValue(lensIntrinsic.at<double>(1,2) - lensIntrinsic.at<double>(1,1) /2 );

        }
        else
        {
            lensIntrinsic.at<double>(0,0) = float(ui->lensCropX->value());
            lensIntrinsic.at<double>(1,1) = float(ui->lensCropY->value());
            lensIntrinsic.at<double>(0,2) = intrinsic.at<double>(0,0) /2.0 +  float(ui->lensOffsetX->value());
            lensIntrinsic.at<double>(1,2) = intrinsic.at<double>(1,1) /2.0 +  float(ui->lensOffsetY->value());
            QString formattedLensIntrinsics = QString("%1 %2 %3\n%4 %5 %6\n%7 %8 %9")
                    .arg(lensIntrinsic.at<double>(0,0),0,'f',4).arg(lensIntrinsic.at<double>(0,1),0,'f',4).arg(lensIntrinsic.at<double>(0,2),0,'f',4)
                    .arg(lensIntrinsic.at<double>(1,0),0,'f',4).arg(lensIntrinsic.at<double>(1,1),0,'f',4).arg(lensIntrinsic.at<double>(1,2),0,'f',4)
                    .arg(lensIntrinsic.at<double>(2,0),0,'f',4).arg(lensIntrinsic.at<double>(2,1),0,'f',4).arg(lensIntrinsic.at<double>(2,2),0,'f',4);
            ui->lensIntrinsicsText->setText(formattedLensIntrinsics);
            printf("%s",formattedLensIntrinsics.toUtf8().data());
            lensApplyManual();
        }
    }
*/
    if (ui->holdFrameCheckbox->checkState()==Qt::CheckState::Unchecked)
    {
//        cap->read(origImage);
        cap->read(image);

//        if (ui->imageSplitFullRadio->isChecked())
//        {
//            image = origImage;
//            cp->split=0;
//        }
//        if (ui->imageSplitLeftRadio->isChecked())
//        {
//            cv::Rect cropRect(0,0,origImage.cols/2,origImage.rows);
//            origImage(cropRect).copyTo(image);
//            cp->split=1;
//        }
//        if (ui->imageSplitRightRadio->isChecked())
//        {
//            cv::Rect cropRect(origImage.cols/2,0,origImage.cols/2,origImage.rows);
//            origImage(cropRect).copyTo(image);
//            cp->split=2;
//        }
        //update our profile with current capture size.
        cp->captureSize[0]=image.cols;
        cp->captureSize[1]=image.rows;


        if (image.cols > 800)
        {
            ui->video->setMinimumSize(image.cols/2,image.rows/2);
        }
        else
        {
            ui->video->setMinimumSize(image.cols,image.rows);
        }
        printf("Video capture size: %d %d\n",image.cols,image.rows);
        cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
    }
    if (distortionCalculated)
    {
        cv::Mat rvec(3,1,CV_64F,0.0f);
        cv::Mat tvec(3,1,CV_64F,0.0f);
        std::vector<cv::Point2f> distortedPoints;
        //gridPoints.push_back(cv::Point3f((512-intrinsic.at<double>(0,2))/intrinsic.at<double>(0,0), (200 - intrinsic.at<double>(1, 2))/ intrinsic.at<double>(1,1), 1));

        if (staticGridPoints.size() != (staticGridSize+1) * (staticGridSize+1))
        {
            staticGridPoints.clear();
            for (uint8_t i=0; i<=staticGridSize; i++)
            {
                for (uint8_t j=0; j<=staticGridSize; j++)
                {
                    staticGridPoints.push_back(cv::Point3f( (i / float(staticGridSize)) - 0.5f,(j / float(staticGridSize)) - 0.5f,1.0f));
                }
            }
        }
        cv::Mat ident = (cv::Mat_<double>(3,3) << 1, 0, 0, 0, 1, 0, 0, 0, 1);
        //cv::Mat optimalIntrinsics =cv::getOptimalNewCameraMatrix(intrinsic,distCoeffs,cv::Size(image.cols,image.rows),1.0f);
        cv::Mat optimalIntrinsics;
        cv::fisheye::estimateNewCameraMatrixForUndistortRectify(intrinsic,distCoeffs,cv::Size(image.cols,image.rows),ident,optimalIntrinsics,ui->balanceSpinBox->value(),cv::Size(image.cols,image.rows),1.0) ;//  getOptimalNewCameraMatrix(intrinsic,distCoeffs,cv::Size(image.cols,image.rows),1.0f);
        if (! ui->optimiseCheckbox->isChecked())
        {
              optimalIntrinsics = intrinsic;
        }
        cv::fisheye::undistortImage(image, undistortedImage, intrinsic, distCoeffs,optimalIntrinsics,cv::Size(image.cols,image.rows));
        QString formattedOptimalIntrinsics = QString("%1 %2 %3\n%4 %5 %6\n%7 %8 %9")
                .arg(optimalIntrinsics.at<double>(0,0),0,'f',4).arg(optimalIntrinsics.at<double>(0,1),0,'f',4).arg(optimalIntrinsics.at<double>(0,2),0,'f',4)
                .arg(optimalIntrinsics.at<double>(1,0),0,'f',4).arg(optimalIntrinsics.at<double>(1,1),0,'f',4).arg(optimalIntrinsics.at<double>(1,2),0,'f',4)
                .arg(optimalIntrinsics.at<double>(2,0),0,'f',4).arg(optimalIntrinsics.at<double>(2,1),0,'f',4).arg(optimalIntrinsics.at<double>(2,2),0,'f',4);
        ui->optIntrinsicsText->setPlainText(formattedOptimalIntrinsics);
        for (uint8_t i=0;i<INTRINSICS_SIZE;i++)
        {
            cp->optimalIntrinsics[i]=optimalIntrinsics.at<double>(i/3,i%3);
        }
        ui->video->clearGridPoints();
        ui->video->setGridSize(staticGridSize+1);
        //if (ui->cameraCalibrationModeRadio->isChecked())
        //{
            cv::fisheye::projectPoints(staticGridPoints,distortedPoints,rvec,tvec,intrinsic,distCoeffs);
            for (uint32_t i=0;i<distortedPoints.size();i++)
            {
                ui->video->addGridPoint(0.5 + (distortedPoints[i].x - intrinsic.at<double>(0,2)) /intrinsic.at<double>(0,0),0.5 + (distortedPoints[i].y - intrinsic.at<double>(1,2))/intrinsic.at<double>(1,1));
                //printf("adding point  %f %f -> %f %f\n",staticGridPoints[i].x,staticGridPoints[i].y,distortedPoints[i].x,distortedPoints[i].y);
            }
        //}
        //else //lens distortion
        //{
        //    if (staticGridPoints.size() > 0 && lensIntrinsic.rows > 0)
        //    {
        //        cv::fisheye::projectPoints(staticGridPoints,distortedPoints,rvec,tvec,lensIntrinsic,lensDistCoeffs);
        //        for (uint32_t i=0;i<distortedPoints.size();i++)
        //        {
        //            ui->video->addGridPoint(0.5 + (distortedPoints[i].x - lensIntrinsic.at<double>(0,2)) /lensIntrinsic.at<double>(0,0),0.5 + (distortedPoints[i].y - lensIntrinsic.at<double>(1,2))/lensIntrinsic.at<double>(1,1));
        //            //printf("adding point  %f %f -> %f %f\n",staticGridPoints[i].x,staticGridPoints[i].y,distortedPoints[i].x,distortedPoints[i].y);
        //        }
        //    }
        //}

        if ( ui->undistortedCheckbox->isChecked())
        {
            ui->video->setImage(image.data,image.cols,image.rows,image.channels());
        }
        else
        {
            //if (ui->cameraCalibrationModeRadio->isChecked())
            //{
                ui->video->setImage(undistortedImage.data,undistortedImage.cols,undistortedImage.rows,undistortedImage.channels());
            //}
            //else
            //{
                //show lens image
                //crop our image
            //    cv::Rect roi;
            //    roi.x = lensIntrinsic.at<double>(0,2) - lensIntrinsic.at<double>(0,0) /2;
            //    roi.y = lensIntrinsic.at<double>(1,2) - lensIntrinsic.at<double>(1,1) /2;
            //    roi.width = lensIntrinsic.at<double>(0,0);
            //    roi.height = lensIntrinsic.at<double>(1,1);

            //    if (roi.x < 0)
            //    {
            //        roi.width += roi.x;
            //        roi.x = 0;
            //    }

            //    if (roi.x + roi.width > undistortedImage.cols)
            //    {
            //        roi.width = undistortedImage.cols - roi.x;
            //    }

            //    if (roi.y < 0)
            //    {
            //        roi.height += roi.y;
            //        roi.y = 0;
            //    }

            //    if (roi.y + roi.height > undistortedImage.rows)
            //    {
            //        roi.height = undistortedImage.rows - roi.y;
            //    }

            //    /* Crop the original image to the defined ROI */
            //    cv::Mat croppedUndistortedImage;
            //    cv::fisheye::undistortImage(undistortedImage, croppedUndistortedImage, lensIntrinsic, lensDistCoeffs,lensIntrinsic,cv::Size(undistortedImage.cols,undistortedImage.rows));
            //     croppedUndistortedImage(roi).copyTo(lensUndistortedImage);
            //    ui->video->setImage(lensUndistortedImage.data,lensUndistortedImage.cols,lensUndistortedImage.rows,lensUndistortedImage.channels());
            //}
        }
    }

    else
    {
        //no distortion calculated
        ui->video->setImage(image.data,image.cols,image.rows,image.channels());
    }
}
    if (uiDirty)
    {

        ui->intrinsicsText->setText(formattedIntrinsics);
        ui->distortionText->setText(formattedDistortion);
        ui->reprojectionErrorLabel->setText(QString("%1").arg(cp->reprojectionError,0,'f',4));


        uiDirty=false;
    }
    captureMutex.unlock();
}

void DistortionToolsWindow::savePNG()
{
    QImage qi = QImage(*(ui->video->videoImage));
    QString fileName = QFileDialog::getSaveFileName(this,"Save PNG Image", QApplication::applicationFilePath(), "Image Files (*.png)");
    if (fileName.compare("") != 0)
    {
        qi.save(fileName);
        ui->statusBar->showMessage(QString("Saved image as %1").arg(fileName), 5000);
    }
}

void DistortionToolsWindow::addCameraProfile()
{
    ui->statusBar->showMessage("Multiple Camera Profiles not implemented yet.", 5000);
}

void DistortionToolsWindow::showGridOverlay(int i)
{
    ui->video->showGridOverlay=false;
    if (i > 0)
    {
        ui->video->showGridOverlay=true;
    }
}
/*void DistortionToolsWindow::lensCaptureNextImage()
{
    //distortionCalculated=false;
    if (lensImagesCaptured == 0)
    {
        lensObjectPoints.clear();
    }

    cv::Mat tempImage;
    cv::Mat boardPoints;
    cv::cvtColor(lensUndistortedImage,tempImage,cv::COLOR_RGB2GRAY);
    bool found = cv::findChessboardCorners( tempImage, boardSize, boardPoints, cv::CALIB_CB_ADAPTIVE_THRESH );
    if (found)
    {
        ui->statusBar->showMessage("Chessboard pattern found in lens image.",2000);
        cv::cornerSubPix(tempImage, boardPoints, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(2, 30, 0.1));
        lensImagePoints.push_back(boardPoints);
        lensObjectPoints.push_back(lensFixedChessboardPoints);
        lensImagesCaptured++;
        ui->imagesCapturedLabel->setText(QString("Lens images captured: %1").arg(lensImagesCaptured));
    }
    else
    {
        ui->statusBar->showMessage("Could not find Chessboard pattern in lens image.",2000);
    }
}*/


/*void DistortionToolsWindow::lensCalculateDistortion()
{
    cv::Mat rvec(3,1,CV_64F,0.0f);
    cv::Mat tvec(3,1,CV_64F,0.0f);

    float rpError = cv::fisheye::calibrate(lensObjectPoints, lensImagePoints, lensUndistortedImage.size(), lensIntrinsic, lensDistCoeffs, rvec, tvec);

    QString formattedDistortion = QString("%1,%2,%3,%4") //,%5")
            .arg(lensDistCoeffs.at<double>(0),0,'f',4)
            .arg(lensDistCoeffs.at<double>(1),0,'f',4)
            .arg(lensDistCoeffs.at<double>(2),0,'f',4)
            .arg(lensDistCoeffs.at<double>(3),0,'f',4);
            //.arg(lensDistCoeffs.at<double>(4),0,'f',4);

    ui->lensDistortionText->setText(formattedDistortion);


}*/

bool ProfileSerialiser::fromString(std::vector<CameraProfile>* cameraProfiles, QString s)
{
    cameraProfiles->clear();
    QJsonParseError e;
    QJsonDocument d = QJsonDocument::fromJson(s.toUtf8(), &e);
    if (! d.isNull())
    {
        if (d.isObject())
        {
            QJsonObject json = d.object();
            QJsonArray profiles = json["profiles"].toArray();
            for (uint32_t i = 0;i< profiles.size();i++)
            {
                QJsonObject j = profiles[i].toObject();
                CameraProfile cp;
                QString ns = j["name"].toString();
                strncpy(cp.name,ns.toUtf8().data(),64);
                cp.name[63] = 0; //zero-terminate
                cp.reprojectionError = j["reprojectionError"].toDouble();
                QJsonArray intrinsicsArray= j["intrinsics"].toArray();
                if (intrinsicsArray.size() != INTRINSICS_SIZE)
                {
                    printf("ERROR: intrinsic data malformed.\n");
                    exit(0);
                }
                for (uint32_t k=0;k<INTRINSICS_SIZE;k++)
                {
                    cp.intrinsics[k]=intrinsicsArray[k].toDouble();
                }
                QJsonArray distortionArray= j["distortion"].toArray();
                if (distortionArray.size() != DISTORTION_SIZE)
                {
                    printf("ERROR: distortion data malformed.\n");
                    exit(0);
                }
                for (uint32_t k=0;k<DISTORTION_SIZE;k++)
                {
                    cp.distortionCoeffs[k]=distortionArray[k].toDouble();
                }
                QJsonArray capSizeArray= j["captureSize"].toArray();
                cp.captureSize[0] = capSizeArray[0].toDouble();
                cp.captureSize[1] = capSizeArray[1].toDouble();
                cp.split = j["split"].toInt();
                cameraProfiles->push_back(cp);


            }
        }
        else
        {
            printf("ERROR: Not a JSON Object.\n");
            exit(0);
        }
        return true;
    }
    printf("ERROR: JSONDocument Error: %s\n",e.errorString().toUtf8().data());
    return false;
}

QString ProfileSerialiser::toString(std::vector<CameraProfile>* cameraProfiles)
{
    QJsonObject json;
    QJsonArray profilesArray;
    for (uint32_t i=0;i<cameraProfiles->size();i++)
    {
        CameraProfile cp = cameraProfiles->at(i);
        QJsonObject profileObject;
        QJsonArray intrinsicsArray;
        QJsonArray optIntrinsicsArray;
        QJsonArray distortionArray;
        QJsonArray capSizeArray;
        profileObject.insert("name",QJsonValue(QString(cp.name)));
        for (uint32_t k=0;k<INTRINSICS_SIZE;k++)
        {
            intrinsicsArray.push_back(QJsonValue(cp.intrinsics[k]));
        }
        for (uint32_t k=0;k<INTRINSICS_SIZE;k++)
        {
            optIntrinsicsArray.push_back(QJsonValue(cp.optimalIntrinsics[k]));
        }
        for (uint32_t k=0;k<DISTORTION_SIZE;k++)
        {
            distortionArray.push_back(QJsonValue(cp.distortionCoeffs[k]));
        }
        capSizeArray.push_back(cp.captureSize[0]);
        capSizeArray.push_back(cp.captureSize[1]);

        profileObject.insert("intrinsics",intrinsicsArray);
        profileObject.insert("optimal_intrinsics",optIntrinsicsArray);

        profileObject.insert("distortion",distortionArray);
        profileObject.insert("reprojectionError",QJsonValue(cp.reprojectionError));
        profileObject.insert("captureSize",capSizeArray);
        profileObject.insert("split",QJsonValue(cp.split));
        profilesArray.push_back(profileObject);
    }
    json.insert("profiles",profilesArray);
    QJsonDocument d(json);
    return QString(d.toJson());
}
