#-------------------------------------------------
#
# Project created by QtCreator 2018-06-09T21:08:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DistortionTools
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        distortiontoolswindow.cpp \
    distortionwidget.cpp \
    displayopenhmd.cpp

HEADERS += \
        distortiontoolswindow.h \
        distortionwidget.h \
    displayopenhmd.h

INCLUDEPATH += /usr/local/include
INCLUDEPATH += /usr/local/include/opencv4
LIBS += -L/usr/local/lib

LIBS += -lopencv_core -lopencv_imgproc -lopencv_video -lopencv_videoio -lopencv_highgui -lopencv_calib3d

FORMS += \
        distortiontoolswindow.ui

copyimages.commands = $(COPY_DIR) $$PWD/images $$OUT_PWD
first.depends = $(first) copyimages
export(first.depends)
export(copyimages.commands)
QMAKE_EXTRA_TARGETS += first copyimages
